# DFL ColorUtils

This library provides an implementation of the cylindrical color space model characterized by Hue-Chroma-Luma.
The HCL (or HCY) color space is better suited to calculate contrast ratios than the sRGB color space. The library
also contains some functions that are useful in creating colors with good contrast ratios.


### Dependencies:
* <tt>Qt Core (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>

### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/colorutils dfl-colorutils`
- Enter the `dfl-colorutils` folder
  * `cd dfl-colorutils`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release -Duse_qt_version=qt5`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any other feature you request for... :)
