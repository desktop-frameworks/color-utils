/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * This file has been taken from KDE project for use with DFL.
 * Suitable modifications have been done to suit our needs.
 *
 * Original Copyright and License:
 *   SPDX-FileCopyrightText: 2007 Matthew Woehlke <mw_triad@users.sourceforge.net>
 *   SPDX-FileCopyrightText: 2007 Olaf Schmidt <ojschmidt@kde.org>
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 *
 * The DFL::ColorUtils::HCYColor class implements the cylindrical color space model
 * characterized by Hue-Chroma-Luma. The HCL (or HCY) color space is better suited
 * to calculate contrast ratios than the sRGB color space.
 **/

#pragma once

#include <QColor>

namespace DFL {
    namespace ColorUtils {
        class HCYColor;
    }
}

class DFL::ColorUtils::HCYColor {
    public:
        explicit HCYColor( const QColor& );
        explicit HCYColor( qreal h_, qreal c_, qreal y_, qreal a_ = 1.0 );
        QColor qColor() const;
        QColor shade( qreal ky, qreal kc );

        qreal h, c, y, a;

        static qreal hue( const QColor& );
        static qreal chroma( const QColor& );
        static qreal luma( const QColor& );

    private:
        static qreal gamma( qreal );
        static qreal igamma( qreal );
        static qreal lumag( qreal, qreal, qreal );
};
