/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * This file has been taken from KDE project for use with DFL.
 * Suitable modifications have been done to suit our needs.
 *
 * Original Copyright and License:
 *   SPDX-FileCopyrightText: 2007 Matthew Woehlke <mw_triad@users.sourceforge.net>
 *   SPDX-FileCopyrightText: 2007 Olaf Schmidt <ojschmidt@kde.org>
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 *
 * The DFL::ColorUtils namespace contains some functions that are useful in
 * creating colors with good contrast ratios. These functions make use of the
 * HCL Color Space.
 **/

#include "DFColorSpace.hpp"
#include "DFColorUtils.hpp"

#include <QColor>
#include <QImage>
#include <QtNumeric> // qIsNaN

#include <math.h>

static inline qreal normalize( qreal a ) {
    return (a < 1.0 ? (a > 0.0 ? a : 0.0) : 1.0);
}


// BEGIN internal helper functions
static inline qreal mixQreal( qreal a, qreal b, qreal bias ) {
    return a + (b - a) * bias;
}


// END internal helper functions

qreal DFL::ColorUtils::hue( const QColor& color ) {
    return DFL::ColorUtils::HCYColor::hue( color );
}


qreal DFL::ColorUtils::chroma( const QColor& color ) {
    return DFL::ColorUtils::HCYColor::chroma( color );
}


qreal DFL::ColorUtils::luma( const QColor& color ) {
    return DFL::ColorUtils::HCYColor::luma( color );
}


void DFL::ColorUtils::getHcy( const QColor& color, qreal *h, qreal *c, qreal *y, qreal *a ) {
    if ( !c || !h || !y ) {
        return;
    }

    DFL::ColorUtils::HCYColor khcy( color );

    *c = khcy.c;
    *h = khcy.h + (khcy.h < 0.0 ? 1.0 : 0.0);
    *y = khcy.y;

    if ( a ) {
        *a = khcy.a;
    }
}


QColor DFL::ColorUtils::hcyColor( qreal h, qreal c, qreal y, qreal a ) {
    return DFL::ColorUtils::HCYColor( h, c, y, a ).qColor();
}


static qreal contrastRatioForLuma( qreal y1, qreal y2 ) {
    if ( y1 > y2 ) {
        return (y1 + 0.05) / (y2 + 0.05);
    }
    else {
        return (y2 + 0.05) / (y1 + 0.05);
    }
}


qreal DFL::ColorUtils::contrastRatio( const QColor& c1, const QColor& c2 ) {
    return contrastRatioForLuma( luma( c1 ), luma( c2 ) );
}


QColor DFL::ColorUtils::lighten( const QColor& color, qreal ky, qreal kc ) {
    DFL::ColorUtils::HCYColor c( color );

    c.y = 1.0 - normalize( (1.0 - c.y) * (1.0 - ky) );
    c.c = 1.0 - normalize( (1.0 - c.c) * kc );
    return c.qColor();
}


QColor DFL::ColorUtils::darken( const QColor& color, qreal ky, qreal kc ) {
    DFL::ColorUtils::HCYColor c( color );

    c.y = normalize( c.y * (1.0 - ky) );
    c.c = normalize( c.c * kc );
    return c.qColor();
}


QColor DFL::ColorUtils::shade( const QColor& color, qreal ky, qreal kc ) {
    DFL::ColorUtils::HCYColor c( color );

    c.y = normalize( c.y + ky );
    c.c = normalize( c.c + kc );
    return c.qColor();
}


static QColor tintHelper( const QColor& base, qreal baseLuma, const QColor& color, qreal amount ) {
    DFL::ColorUtils::HCYColor result( DFL::ColorUtils::mix( base, color, pow( amount, 0.3 ) ) );

    result.y = mixQreal( baseLuma, result.y, amount );

    return result.qColor();
}


QColor DFL::ColorUtils::tint( const QColor& base, const QColor& color, qreal amount ) {
    if ( amount <= 0.0 ) {
        return base;
    }

    if ( amount >= 1.0 ) {
        return color;
    }

    if ( qIsNaN( amount ) ) {
        return base;
    }

    qreal  baseLuma = luma( base ); // cache value because luma call is expensive
    double ri       = contrastRatioForLuma( baseLuma, luma( color ) );
    double rg       = 1.0 + ( (ri + 1.0) * amount * amount * amount);
    double u        = 1.0;
    double l        = 0.0;
    QColor result;

    for (int i = 12; i; --i) {
        double a = 0.5 * (l + u);
        result = tintHelper( base, baseLuma, color, a );
        double ra = contrastRatioForLuma( baseLuma, luma( result ) );

        if ( ra > rg ) {
            u = a;
        }
        else {
            l = a;
        }
    }
    return result;
}


QColor DFL::ColorUtils::mix( const QColor& c1, const QColor& c2, qreal bias ) {
    if ( bias <= 0.0 ) {
        return c1;
    }

    if ( bias >= 1.0 ) {
        return c2;
    }

    if ( qIsNaN( bias ) ) {
        return c1;
    }

    qreal r = mixQreal( c1.redF(), c2.redF(), bias );
    qreal g = mixQreal( c1.greenF(), c2.greenF(), bias );
    qreal b = mixQreal( c1.blueF(), c2.blueF(), bias );
    qreal a = mixQreal( c1.alphaF(), c2.alphaF(), bias );

    return QColor::fromRgbF( r, g, b, a );
}


QColor DFL::ColorUtils::overlayColors( const QColor& base, const QColor& paint, QPainter::CompositionMode comp ) {
    // This isn't the fastest way, but should be "fast enough".
    // It's also the only safe way to use QPainter::CompositionMode
    QImage   img( 1, 1, QImage::Format_ARGB32_Premultiplied );
    QPainter p( &img );
    QColor   start = base;

    start.setAlpha( 255 ); // opaque
    p.fillRect( 0, 0, 1, 1, start );
    p.setCompositionMode( comp );
    p.fillRect( 0, 0, 1, 1, paint );
    p.end();
    return img.pixel( 0, 0 );
}
